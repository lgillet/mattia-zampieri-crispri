rm(list = ls())
gc()

# Load packages 
library(RColorBrewer)
library(gridExtra)
library(ComplexHeatmap)
library(tidyverse)
library(ggrepel)

getwd()
setwd("Y:/lgillet/Experiments/LG005_Mattia_ecoli/200922_Dataset2")

## Definiting inputs
file.name <- "./data/20201001_171309_Mattia_Exploris_Report.xls"
annotation.file <- "./data/SampleAnnotation2.txt"
fasta.file <- "./data/UniProt_ecoli_200422.fasta"

##### functions (from JP)

clean_fread_tibble<- function (filename) 
{
  data.table::fread(filename) %>% janitor::clean_names() %>% 
    tibble::as_tibble()
}

median_normalisation <- function (data, file_column_name, intensity_column_name, na.rm = FALSE) {
  median_run_intensity <- median_intensity <- NULL
  data %>% 
    dplyr::group_by({{file_column_name}}) %>% 
    dplyr::mutate(median_run_intensity = stats::median({{intensity_column_name}}, na.rm = na.rm)) %>% 
    dplyr::ungroup() %>% 
    dplyr::mutate(median_intensity = stats::median(unique(median_run_intensity), na.rm = na.rm)) %>% 
    dplyr::mutate(normalised_intensity_log2 = {{intensity_column_name}}/median_run_intensity * median_intensity) %>%
    dplyr::select(-median_run_intensity, -median_intensity)
}

find_peptide <- function(data, sequence, peptide){
  data%>%
    mutate(start = str_locate({{sequence}}, {{peptide}})[,1], end = str_locate({{sequence}}, {{peptide}})[,2])%>%
    mutate(aa_before = str_sub({{sequence}}, start = start-1, end = start-1))%>%
    mutate(last_aa = str_sub({{sequence}}, start = end, end = end))
}

peptide_type <- function(data, aa_before, last_aa){
  data%>%
    mutate(N_term_tryp = ifelse({{aa_before}} == "" | {{aa_before}} == "K" | {{aa_before}} == "R", TRUE, FALSE ))%>%
    mutate(C_term_tryp = ifelse({{last_aa}} == "K" | {{last_aa}} == "R" | {{last_aa}} == "", TRUE, FALSE ))%>%
    mutate(pep_type = case_when(
      N_term_tryp + C_term_tryp == 2 ~ "fully-tryptic",
      N_term_tryp + C_term_tryp == 1 ~ "semi-tryptic",
      N_term_tryp + C_term_tryp == 0 ~ "non-tryptic"
    ))%>%
    dplyr::select(-N_term_tryp, -C_term_tryp)
}

filter_qvalue <- function (data, peptide_column_name, condition_column_name, qvalue_column_name, min_qvalue, min_values_per_condition, in_min_conditions) {
  peptide_list <- {{data}} %>%
    dplyr::group_by({{peptide_column_name}}, {{condition_column_name}}) %>%
    dplyr::summarise(qvalue_below_count = sum({{qvalue_column_name}} <= {{min_qvalue}})) %>% 
    dplyr::filter(qvalue_below_count >= {{min_values_per_condition}}) %>% # at least with "min_values_per_conditions" measurements
    dplyr::group_by(eg_precid) %>%
    dplyr::count(qvalue_below_count) %>% 
    dplyr::summarise(n2_and_n3 = sum(n)) %>% 
    dplyr::filter(n2_and_n3 >= {{in_min_conditions}}) %>% # in at least 2 conditions 
    dplyr::pull({{peptide_column_name}})
  dplyr::filter({{data}}, {{peptide_column_name}} %in% peptide_list)
}


# Read files
DIA_spectronaut <- clean_fread_tibble(file.name)
colnames(DIA_spectronaut)
unique(DIA_spectronaut$r_file_name)
# sample.annotation2 <- read.delim2(annotation.file2,dec=".",sep="\t",header=TRUE, as.is = TRUE)

sample.annotation <- read.delim2(annotation.file,dec=".",sep="\t",header=TRUE, as.is = TRUE)
colnames(sample.annotation)
length(unique(sample.annotation$r_file_name))
length(unique(sample.annotation$r_condrep))

proteome <- phylotools::read.fasta(fasta.file) %>% janitor::clean_names() %>% 
  dplyr::mutate(uniprot_id = stringr::str_extract(seq_name, "\\|.*?\\|") %>% stringr::str_extract("(\\w){1,}")) %>% 
  dplyr::mutate(sequence = seq_text) %>% dplyr::select(-seq_name, -seq_text)

DIA_spectronaut_clean <- DIA_spectronaut %>%
  filter(eg_is_decoy == "FALSE") %>%
  mutate(fg_quantity = ifelse(fg_ms2raw_quantity < 10, NA, fg_ms2raw_quantity)) %>%
  filter(!is.na(fg_quantity)) %>%
  mutate(intensity_log2 = log2(fg_quantity)) %>%
  median_normalisation(
    file_column_name = r_file_name,
    intensity_column_name = intensity_log2,
    na.rm = TRUE
  ) %>%
  mutate(uniprot_id = pg_protein_accessions) %>%
  left_join(proteome, by = "uniprot_id") %>%
  find_peptide(sequence, pep_stripped_sequence) %>%
  peptide_type(aa_before, last_aa) %>%
  mutate(length = str_length(sequence))%>%
  mutate(eg_precid = paste(eg_modified_sequence, fg_charge, sep = "")) %>%
  select(-r_condition, -r_replicate) %>% 
  left_join(sample.annotation, by = "r_file_name") %>% 
  filter_qvalue(., eg_precid, r_condition, eg_qvalue, 1E-5, min_values_per_condition = 3, in_min_conditions = 1) %>%
  filter_qvalue(., eg_precid, r_condition, eg_qvalue, 1E-5, min_values_per_condition = 2, in_min_conditions = 3)

colnames(DIA_spectronaut_clean)

# test <- unique(DIA_spectronaut_clean[,c("r_file_name", "r_condition", "r_replicate", "r_condrep")])
length(unique(DIA_spectronaut_clean$r_file_name))
length(unique(DIA_spectronaut_clean$r_condrep))
unique(DIA_spectronaut_clean$r_condrep)
# set up the r_condition and r_condrep orders:
r_condrep_order <- c("Mock_Lip_1", "Mock_Lip_2", "Mock_Lip_3", "Mock2_Lip_1", "Mock2_Lip_2", "Mock2_Lip_3", "Chloroxine0.01_Lip_1", "Chloroxine0.01_Lip_2", "Chloroxine0.01_Lip_3", "Chloroxine0.1_Lip_1", "Chloroxine0.1_Lip_2", "Chloroxine0.1_Lip_3", "Chloroxine1_Lip_1", "Chloroxine1_Lip_2", "Chloroxine1_Lip_3", "Chloroxine10_Lip_1", "Chloroxine10_Lip_2", "Chloroxine10_Lip_3", "Tiabendozole40_Lip_1", "Tiabendozole40_Lip_2", "Tiabendozole40_Lip_3", "Suloctidil20_Lip_1", "Suloctidil20_Lip_2", "Suloctidil20_Lip_3", "Thiethylperazine20_Lip_1", "Thiethylperazine20_Lip_2", "Thiethylperazine20_Lip_3", "Tegaserod20_Lip_1", "Tegaserod20_Lip_2", "Tegaserod20_Lip_3")
DIA_spectronaut_clean <- subset(DIA_spectronaut_clean, r_condrep %in% r_condrep_order)
unique(DIA_spectronaut_clean$r_condrep)
DIA_spectronaut_clean$r_condrep <- factor(DIA_spectronaut_clean$r_condrep, levels = r_condrep_order)
unique(DIA_spectronaut_clean$r_condrep)

r_condition_order <- c("Mock_Lip", "Mock2_Lip", "Chloroxine10_Lip", "Chloroxine1_Lip", "Chloroxine0.1_Lip", "Chloroxine0.01_Lip", "Tiabendozole40_Lip", "Suloctidil20_Lip", "Thiethylperazine20_Lip", "Tegaserod20_Lip")
unique(DIA_spectronaut_clean$r_condition)
DIA_spectronaut_clean$r_condition <- factor(DIA_spectronaut_clean$r_condition, levels = r_condition_order)
unique(DIA_spectronaut_clean$r_condition)


#Calculate assay fraction / representation in the overall table:
n_condrep <- length(unique(DIA_spectronaut_clean$r_condrep))

eg_precid_percondrep <- DIA_spectronaut_clean %>%
  distinct(r_condrep, eg_precid) %>% 
  add_count(eg_precid, name = "count_precid") %>%
  distinct(r_condrep, eg_precid, count_precid) %>%
  mutate(eg_freq = count_precid / n_condrep) %>%
  mutate(eg_freqbin = cut(eg_freq, breaks = seq(0,1,0.1), right = TRUE)) %>% 
  count(r_condrep, eg_freqbin) %>%
  complete(eg_freqbin, r_condrep) %>%
  mutate(n = replace_na(n, 0))

#Make a palette to have enough colors for plotting
colourCount <- length(unique(eg_precid_percondrep$eg_freqbin))
colourCount
getPalette <- colorRampPalette(brewer.pal(9, "YlOrBr"))

#Plot at assay level
ggplot(eg_precid_percondrep, aes(x = r_condrep, y = n, fill = eg_freqbin)) + 
  geom_bar(stat = 'identity', width = rel(.48), color = 'black', size = 0.05) +
  ggtitle('Number of precursors across runs') +
  theme_bw() + 
  theme(axis.title.y = element_blank(),
        axis.text.y = element_text(size = 5), 
        legend.position = 'bottom') +
  scale_fill_manual(values = getPalette(colourCount), 
                    guide = guide_legend(direction = "horizontal", 
                                         title.position = "left",
                                         label.position = "bottom", 
                                         label.hjust = 0.5, 
                                         label.vjust = 0.5,
                                         label.theme = element_text(angle = 270), 
                                         nrow = 3)) +
  xlab('r_condrep') + 
  ylab('Number of Precursors') + 
  theme(axis.text.x = element_text(angle=90)) +
  theme(text = element_text(size=10),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.box="vertical",
        legend.position="right",
        legend.text = element_text(size=6))

ggsave("./fig_output_201215/number_of_prec_all_runs.pdf", width = 18.5, height = 12, units = 'cm')


#Calculate protein fraction / representation in the overall table:
prot_percondrep <- DIA_spectronaut_clean %>%
  distinct(r_condrep, pg_protein_groups) %>% 
  add_count(pg_protein_groups, name = "count_pg_protein_groups") %>%
  distinct(r_condrep, pg_protein_groups, count_pg_protein_groups) %>%
  mutate(pg_freq = count_pg_protein_groups / n_condrep) %>%
  mutate(pg_freqbin = cut(pg_freq, breaks = seq(0,1,0.1), right = TRUE)) %>% 
  count(r_condrep, pg_freqbin) %>%
  complete(pg_freqbin, r_condrep) %>%
  mutate(n = replace_na(n, 0))

#Make a palette to have enough colors for plotting
colourCount <- length(unique(prot_percondrep$pg_freqbin))
colourCount
getPalette <- colorRampPalette(brewer.pal(9, "YlOrBr"))

#Plot at assay level
ggplot(prot_percondrep, aes(x = r_condrep, y = n, fill = pg_freqbin)) + 
  geom_bar(stat = 'identity', width = rel(.48), color = 'black', size = 0.05) +
  ggtitle('Number of protein groups across runs') +
  theme_bw() + 
  theme(axis.title.y = element_blank(),
        axis.text.y = element_text(size = 5), 
        legend.position = 'bottom') +
  scale_fill_manual(values = getPalette(colourCount), 
                    guide = guide_legend(direction = "horizontal", 
                                         title.position = "left",
                                         label.position = "bottom", 
                                         label.hjust = 0.5, 
                                         label.vjust = 0.5,
                                         label.theme = element_text(angle = 270), 
                                         nrow = 3)) +
  xlab('r_condrep') + 
  ylab('Number of Protein groups') + 
  theme(axis.text.x = element_text(angle=90)) +
  theme(text = element_text(size=10),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.box="vertical",
        legend.position="right",
        legend.text = element_text(size=6))

ggsave("./fig_output_201215/number_of_prot_all_runs.pdf", width = 18.5, height = 12, units = 'cm')



# plot the fully tryptic/semi tryptic per run
eg_precid_percond2 <- DIA_spectronaut_clean %>%
  group_by(r_condrep, pep_type) %>% 
  add_count(pep_type, name = "count_pep_type") %>%
  select(r_condrep, pep_type, count_pep_type) %>% 
  complete(pep_type, r_condrep) %>%
  distinct(r_condrep, pep_type, count_pep_type) %>%
  mutate(pep_type = replace_na(pep_type, "not_proteotypic"))

unique(eg_precid_percond2$pep_type)
eg_precid_percond2$pep_type <- factor(eg_precid_percond2$pep_type, levels = c("not_proteotypic", "non-tryptic", "semi-tryptic", "fully-tryptic"))

#Make a palette to have enough colors for plotting
colourCount <- length(unique(eg_precid_percond2$pep_type))
colourCount
getPalette <- colorRampPalette(brewer.pal(9, "Spectral"))



#Plot at assay level
ggplot(eg_precid_percond2, aes(x = r_condrep, y = count_pep_type, fill = pep_type)) + 
  geom_bar(stat = 'identity', width = rel(.48), color = 'black', size = 0.05) +
  ggtitle('Distribution of peptide types per run') +
  theme_bw() + 
  theme(axis.title.y = element_blank(),
        axis.text.y = element_text(size = 5), 
        legend.position = 'bottom') +
  scale_fill_manual(values = getPalette(colourCount), 
                    guide = guide_legend(direction = "horizontal", 
                                         title.position = "left",
                                         label.position = "bottom", 
                                         label.hjust = 0.5, 
                                         label.vjust = 0.5,
                                         label.theme = element_text(angle = 270), 
                                         nrow = 1)) +
  xlab('r_condrep') + 
  ylab('Number of peptide types') + 
  theme(axis.text.x = element_text(angle=90)) +
  theme(text = element_text(size=10),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.box="vertical",
        legend.position="right",
        legend.text = element_text(size=6))

ggsave("./fig_output_201215/number_of_peptidetypes_all_runs.pdf", width = 18.5, height = 12, units = 'cm')


# check the effect of the normalization
DIA_spectronaut_clean %>%
  distinct(eg_precid, r_condrep, intensity_log2) %>%
  ggplot(aes(x = r_condrep, y = intensity_log2)) + 
  geom_boxplot() +
  ggtitle('Original distribution of data') +
  theme_bw() + 
  theme(axis.title.y = element_blank(),
        axis.text.y = element_text(size = 5), 
        legend.position = 'bottom') +
  scale_fill_manual(values = getPalette(10), 
                    guide = guide_legend(direction = "horizontal", 
                                         title.position = "left",
                                         label.position = "bottom", 
                                         label.hjust = 0.5, 
                                         label.vjust = 0.5,
                                         label.theme = element_text(angle = 270), 
                                         nrow = 1)) +
  xlab('r_condrep') + 
  ylab('Log2(Intensity)') + 
  theme(axis.text.x = element_text(angle=90)) +
  theme(text = element_text(size=10),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.box="vertical")

ggsave("./fig_output_201215/boxplot_beforeNorm-ggplot.pdf", width = 18.5, height = 12, units = 'cm')



DIA_spectronaut_clean %>%
  distinct(eg_precid, r_condrep, normalised_intensity_log2) %>%
  ggplot(aes(x = r_condrep, y = normalised_intensity_log2)) + 
  geom_boxplot() +
  ggtitle('Normalized distribution of data') +
  theme_bw() + 
  theme(axis.title.y = element_blank(),
        axis.text.y = element_text(size = 5), 
        legend.position = 'bottom') +
  scale_fill_manual(values = getPalette(10), 
                    guide = guide_legend(direction = "horizontal", 
                                         title.position = "left",
                                         label.position = "bottom", 
                                         label.hjust = 0.5, 
                                         label.vjust = 0.5,
                                         label.theme = element_text(angle = 270), 
                                         nrow = 1)) +
  xlab('r_condrep') + 
  ylab('Log2(Intensity)') + 
  theme(axis.text.x = element_text(angle=90)) +
  theme(text = element_text(size=10),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.box="vertical")

ggsave("./fig_output_201215/boxplot_afterNorm-ggplot.pdf", width = 18.5, height = 12, units = 'cm')


##### plots for quantification
colnames(DIA_spectronaut_clean)
# correlation matrix for the peptide abundances across runs
pepmatrix.norm <- DIA_spectronaut_clean %>%
  dplyr::distinct(eg_precid, r_condrep, normalised_intensity_log2) %>%
  pivot_wider(names_from = r_condrep, values_from = normalised_intensity_log2) %>%
  column_to_rownames("eg_precid")

cr <- cor(as.matrix(na.omit(pepmatrix.norm)), use="complete", method="pearson") # get correlations
my_palette <- colorRampPalette(c("lightyellow", "midnightblue"))(n = 32)
colnames(sample.annotation)

# prepare top-annotation
df <- sample.annotation %>%
  filter(r_condrep %in% r_condrep_order) %>%
  arrange(factor(r_condrep, levels = colnames(pepmatrix.norm))) %>% 
  column_to_rownames("r_condrep") %>% 
  select(c("r_condition", "r_replicate", "Strip"))
rownames(df) == colnames(pepmatrix.norm)
unique(df$r_condition)

ha = HeatmapAnnotation(df=df, col = list(r_condition = c("Mock_Lip" = "green1", "Mock2_Lip" = "green2", "Chloroxine0.01_Lip" = "red1", "Chloroxine0.1_Lip" = "red2", "Chloroxine1_Lip" = "red3", "Chloroxine10_Lip" = "red4", "Tiabendozole40_Lip" = "blue1", "Suloctidil20_Lip" = "blue2", "Thiethylperazine20_Lip" = "blue3", "Tegaserod20_Lip" = "blue4"),
                                         Strip = c("1" = "skyblue", "2" = "royalblue", "3" = "slateblue", "4" = "darkblue", "5" = "blue4")))
ha2 = HeatmapAnnotation(df=df, col = list(r_condition = c("Mock_Lip" = "green1", "Mock2_Lip" = "green2", "Chloroxine0.01_Lip" = "red1", "Chloroxine0.1_Lip" = "red2", "Chloroxine1_Lip" = "red3", "Chloroxine10_Lip" = "red4", "Tiabendozole40_Lip" = "blue1", "Suloctidil20_Lip" = "blue2", "Thiethylperazine20_Lip" = "blue3", "Tegaserod20_Lip" = "blue4"),
                                          Strip = c("1" = "skyblue", "2" = "royalblue", "3" = "slateblue", "4" = "darkblue", "5" = "blue4")), which = "row")


#my_palette2 <- colorRampPalette(brewer.pal(11,"Spectral"))(n = 11)

# heatmap correlation plot
Heatmap(as.matrix(na.omit(cr)), name = "Heatmap", show_row_names = FALSE, col=my_palette, 
        cluster_rows = TRUE, cluster_columns = TRUE,
        # clustering_distance_rows = "euclidean", clustering_method_rows = "complete",
        # clustering_distance_columns = "euclidean", clustering_method_columns = "complete",
        row_title_gp = gpar(fontsize = 8, fontface = 1), column_title_gp = gpar(fontsize = 8, fontface = 1),
        row_names_gp =gpar(fontsize = 8, fontface = 1) , column_names_gp = gpar(fontsize = 8, fontface = 1), top_annotation = ha) + ha2  

dev.copy2pdf(file="./fig_output_201215/eg_precid_correlations_ComplexHeatmap.pdf")
dev.off()


Heatmap(as.matrix(na.omit(pepmatrix.norm)), name = "Heatmap", show_row_names = FALSE, col=my_palette, 
        cluster_rows = TRUE, cluster_columns = TRUE,
        row_title_gp = gpar(fontsize = 8, fontface = 1), column_title_gp = gpar(fontsize = 10, fontface = 1),
        row_names_gp =gpar(fontsize = 8, fontface = 1) , column_names_gp = gpar(fontsize = 10, fontface = 1), top_annotation = ha)  

dev.copy2pdf(file="./fig_output_201215/precid_int_ComplexHeatmap_naomit.pdf")
dev.off()

distance_na_to_zero <-function(x, method = "euclidean") {
  y =  x
  y[is.na(x)] = 0					
  distance = dist(y, method = method)
  return(distance)
}

Heatmap(as.matrix(pepmatrix.norm), name = "Heatmap", show_row_names = FALSE, col=my_palette, 
        cluster_rows = TRUE, cluster_columns = TRUE, na_col = "white",
        clustering_distance_rows = function(m) distance_na_to_zero(m), 
        clustering_distance_columns = function(m) distance_na_to_zero(m),
        row_title_gp = gpar(fontsize = 8, fontface = 1), column_title_gp = gpar(fontsize = 10, fontface = 1),
        row_names_gp =gpar(fontsize = 8, fontface = 1) , column_names_gp = gpar(fontsize = 10, fontface = 1), top_annotation = ha)  

dev.copy2pdf(file="./fig_output_201215/precid_int_ComplexHeatmap_wna.pdf")
dev.off()



##### batch effect correction: 
# we calculate the mean value for each peptide/protein in individual batch 
# and subtract it (peptide/protein wise) from each run belonging to individual batch. 
# Last, the global mean across all samples (peptide/protein wise) is added to all runs so that the dynamics of the original data is preserved.
correct_batch_effect <- function (data, peptide_column_name, intensity_column_name, batch_column_name) {
  {{data}} %>% 
    group_by({{peptide_column_name}}) %>% 
    mutate(mean_global = mean({{intensity_column_name}}, na.rm = TRUE)) %>% 
    group_by({{batch_column_name}}, {{peptide_column_name}}) %>% 
    mutate(mean_perbatch = mean({{intensity_column_name}}, na.rm = TRUE)) %>% 
    mutate(pep_corrected = {{intensity_column_name}} - mean_perbatch + mean_global) %>% 
    ungroup() 
}

DIA_spectronaut_clean_corrected <- DIA_spectronaut_clean %>% 
  correct_batch_effect(., eg_precid, normalised_intensity_log2, Strip)

##### heatmap plots checks
# correlation matrix for the peptide abundances across runs
pepmatrix.norm <- DIA_spectronaut_clean_corrected %>%
  dplyr::distinct(eg_precid, r_condrep, pep_corrected) %>%
  pivot_wider(names_from = r_condrep, values_from = pep_corrected) %>%
  column_to_rownames("eg_precid")

cr <- cor(as.matrix(na.omit(pepmatrix.norm)), use="complete", method="pearson") # get correlations
my_palette <- colorRampPalette(c("lightyellow", "midnightblue"))(n = 32)
colnames(sample.annotation)

# prepare top-annotation
df <- sample.annotation %>%
  filter(r_condrep %in% r_condrep_order) %>%
  arrange(factor(r_condrep, levels = colnames(pepmatrix.norm))) %>% 
  column_to_rownames("r_condrep") %>% 
  select(c("r_condition", "r_replicate", "Strip"))
rownames(df) == colnames(pepmatrix.norm)
unique(df$r_condition)

ha = HeatmapAnnotation(df=df, col = list(r_condition = c("Mock_Lip" = "green1", "Mock2_Lip" = "green2", "Chloroxine0.01_Lip" = "red1", "Chloroxine0.1_Lip" = "red2", "Chloroxine1_Lip" = "red3", "Chloroxine10_Lip" = "red4", "Tiabendozole40_Lip" = "blue1", "Suloctidil20_Lip" = "blue2", "Thiethylperazine20_Lip" = "blue3", "Tegaserod20_Lip" = "blue4"),
                                         Strip = c("1" = "skyblue", "2" = "royalblue", "3" = "slateblue", "4" = "darkblue", "5" = "blue4")))
ha2 = HeatmapAnnotation(df=df, col = list(r_condition = c("Mock_Lip" = "green1", "Mock2_Lip" = "green2", "Chloroxine0.01_Lip" = "red1", "Chloroxine0.1_Lip" = "red2", "Chloroxine1_Lip" = "red3", "Chloroxine10_Lip" = "red4", "Tiabendozole40_Lip" = "blue1", "Suloctidil20_Lip" = "blue2", "Thiethylperazine20_Lip" = "blue3", "Tegaserod20_Lip" = "blue4"),
                                          Strip = c("1" = "skyblue", "2" = "royalblue", "3" = "slateblue", "4" = "darkblue", "5" = "blue4")), which = "row")


#my_palette2 <- colorRampPalette(brewer.pal(11,"Spectral"))(n = 11)

# heatmap correlation plot
Heatmap(as.matrix(na.omit(cr)), name = "Heatmap", show_row_names = FALSE, col=my_palette, 
        cluster_rows = TRUE, cluster_columns = TRUE,
        # clustering_distance_rows = "euclidean", clustering_method_rows = "complete",
        # clustering_distance_columns = "euclidean", clustering_method_columns = "complete",
        row_title_gp = gpar(fontsize = 8, fontface = 1), column_title_gp = gpar(fontsize = 8, fontface = 1),
        row_names_gp =gpar(fontsize = 8, fontface = 1) , column_names_gp = gpar(fontsize = 8, fontface = 1), top_annotation = ha) + ha2  

dev.copy2pdf(file="./fig_output_201215/precid_corrected_correlations_ComplexHeatmap.pdf")
dev.off()


Heatmap(as.matrix(na.omit(pepmatrix.norm)), name = "Heatmap", show_row_names = FALSE, col=my_palette, 
        cluster_rows = TRUE, cluster_columns = TRUE,
        row_title_gp = gpar(fontsize = 8, fontface = 1), column_title_gp = gpar(fontsize = 10, fontface = 1),
        row_names_gp =gpar(fontsize = 8, fontface = 1) , column_names_gp = gpar(fontsize = 10, fontface = 1), top_annotation = ha)  

dev.copy2pdf(file="./fig_output_201215/precid_corrected_int_ComplexHeatmap_naomit.pdf")
dev.off()

get_topN_peptides <- function (data, protein_column_name, peptide_column_name, intensity_column_name, top_number) {
  peptide_list <- {{data}} %>%
    dplyr::group_by({{protein_column_name}}, {{peptide_column_name}}) %>%
    dplyr::summarise(mean_int = mean({{intensity_column_name}}, na.rm = TRUE)) %>%
    dplyr::top_n({{top_number}}) %>%     # equivalent to: filter(min_rank(desc(mean_int)) <= 50) 
    dplyr::pull({{peptide_column_name}}) # optional: arrange(pg_protein_groups, desc(mean_int))
  dplyr::filter({{data}}, {{peptide_column_name}} %in% peptide_list)
}

DIA_pep_top50_list <- DIA_spectronaut_clean_corrected %>%
  get_topN_peptides(., pg_protein_groups, eg_precid, pep_corrected, 50) %>% 
  split(.$pg_protein_groups)

plot_function2 <- function(x) {
  ggplot(x, aes(x=r_condrep, y=pep_corrected, group = eg_precid, shape = eg_precid, colour = eg_precid)) +
    ggtitle(unique(x$pg_protein_groups)) +
    geom_line(aes(colour = eg_precid)) +
    geom_point(aes(shape=eg_precid)) +
    scale_shape_manual(values = c(letters, LETTERS)) +
    theme_bw() + 
    theme(legend.position = "right", axis.text.x = element_text(angle = 90))+
    guides(shape = guide_legend(override.aes = list(size=3,linetype=0)))+
    theme(text = element_text(size=10),
          legend.key=element_blank(),
          legend.title=element_blank(),
          legend.box="vertical")}

prot_plots <- map(DIA_pep_top50_list, plot_function2)

ml <- marrangeGrob(prot_plots, nrow = 2, ncol = 1)
ggsave("./fig_output_201215/proteinplots_precid_corrected.pdf", ml, height=20, width=20, units='in', dpi=600)






# cast and re-melt to generate "NA" for the missing peptide intensities => important for ggplot to break the lines when no value
DIA_spectronaut_clean_na <- pepmatrix.norm %>%
  rownames_to_column("eg_precid") %>%
  pivot_longer(-eg_precid, names_to = "r_condrep", values_to = "normalised_intensity_log2") %>%
  left_join(unique(DIA_spectronaut_clean[,c("eg_precid", "pg_protein_groups")]), by = "eg_precid") %>%
  mutate(r_condition = substr(r_condrep, 1, nchar(r_condrep)-2))

# set the r_condition and r_condrep order for DIA_spectronaut_clean_na
unique(DIA_spectronaut_clean_na$r_condrep)
DIA_spectronaut_clean_na$r_condrep <- factor(DIA_spectronaut_clean_na$r_condrep, levels = r_condrep_order)
levels(DIA_spectronaut_clean_na$r_condrep)

unique(DIA_spectronaut_clean_na$r_condition)
DIA_spectronaut_clean_na$r_condition <- factor(DIA_spectronaut_clean_na$r_condition, levels = r_condition_order)
levels(DIA_spectronaut_clean_na$r_condition)

filter_na <- function (data, peptide_column_name, condition_column_name, intensity_column_name, min_values_per_condition, in_min_conditions) {
  peptide_list <- {{data}} %>%
    dplyr::group_by({{peptide_column_name}}, {{condition_column_name}}) %>%
    dplyr::summarise(non_na_count = sum(!is.na({{intensity_column_name}}))) %>% 
    dplyr::filter(non_na_count >= {{min_values_per_condition}}) %>% # at least with "min_values_per_conditions" measurements
    dplyr::group_by(eg_precid) %>%
    dplyr::count(non_na_count) %>% 
    dplyr::summarise(n2_and_n3 = sum(n)) %>% 
    dplyr::filter(n2_and_n3 >= {{in_min_conditions}}) %>% # in at least 2 conditions 
    dplyr::pull({{peptide_column_name}})
  dplyr::filter({{data}}, {{peptide_column_name}} %in% peptide_list)
}

length(unique(DIA_spectronaut_clean_na$r_condition))

DIA_spectronaut_clean_na <- DIA_spectronaut_clean_na %>% 
  filter_na(., eg_precid, r_condition, normalised_intensity_log2, min_values_per_condition = 3, in_min_conditions = 1) %>% 
  filter_na(., eg_precid, r_condition, normalised_intensity_log2, min_values_per_condition = 2, in_min_conditions = 3)

# remove single hits
filter_singlehits <- function (data, protein_column_name, peptide_column_name, min_peptides) {
  protein_list <- {{data}} %>%
    dplyr::group_by({{protein_column_name}}) %>%
    dplyr::distinct({{protein_column_name}}, {{peptide_column_name}}) %>% 
    dplyr::count({{protein_column_name}}) %>% 
    dplyr::filter(n >= {{min_peptides}}) %>% # at least 2 peptides
    dplyr::pull({{protein_column_name}})
  dplyr::filter({{data}}, {{protein_column_name}} %in% protein_list)
}

DIA_spectronaut_clean_na <- DIA_spectronaut_clean_na %>% 
  left_join(unique(DIA_spectronaut_clean[,c("eg_precid", "eg_modified_sequence")]), by = "eg_precid") %>% 
  filter_singlehits(., pg_protein_groups, eg_modified_sequence, min_peptides = 2)


DIA_spectronaut_clean_na_top50_list <- DIA_spectronaut_clean_na %>%
  get_topN_peptides(., pg_protein_groups, eg_precid, normalised_intensity_log2, 50) %>% 
  split(.$pg_protein_groups)

DIA_spectronaut_clean_na_top50_peptides <- DIA_spectronaut_clean_na %>% 
  get_topN_peptides(., pg_protein_groups, eg_precid, normalised_intensity_log2, 30) %>% 
  pull(eg_precid) %>% 
  unique()


plot_precid_per_protein <- function(x) {
  ggplot(x, aes(x=r_condrep, y=normalised_intensity_log2, group = eg_precid, shape = eg_precid, colour = eg_precid)) +
    ggtitle(unique(x$pg_protein_groups)) +
    geom_line(aes(colour = eg_precid)) +
    geom_point(aes(shape=eg_precid, size = 1)) +
    scale_shape_manual(values = c(letters, LETTERS)) +
    theme_bw() +
    theme(legend.position = "right", axis.text.x = element_text(angle = 90))+
    guides(shape = guide_legend(override.aes = list(size=3,linetype=0)))}

prot_plots <- map(DIA_spectronaut_clean_na_top50_list, plot_precid_per_protein)

ml <- marrangeGrob(prot_plots, nrow = 2, ncol = 1)
ggsave("./fig_output_201215/proteinplots_interesting_peptides_AfterNormalization_corrected_filtered.pdf", ml, height=20, width=20, units='in', dpi=600)


DIA_spectronaut_clean_na %>%
  group_by(eg_precid, r_condition) %>%
  mutate(cv_int = sd(2^normalised_intensity_log2, na.rm = TRUE) / mean(2^normalised_intensity_log2, na.rm = TRUE)) %>%
  ggplot(aes(x = r_condition, y = cv_int)) + 
  geom_violin() +
  geom_boxplot(width=0.1) +
  ggtitle('Distribution of CVs across conditions') +
  theme_bw() + 
  theme(axis.title.y = element_blank(),
        axis.text.y = element_text(size = 5), 
        legend.position = 'bottom') +
  scale_fill_manual(values = getPalette(10), 
                    guide = guide_legend(direction = "horizontal", 
                                         title.position = "left",
                                         label.position = "bottom", 
                                         label.hjust = 0.5, 
                                         label.vjust = 0.5,
                                         label.theme = element_text(angle = 270), 
                                         nrow = 1)) +
  xlab('r_condition') + 
  ylab('CV') + 
  theme(axis.text.x = element_text(angle=90)) +
  theme(text = element_text(size=10),
        legend.key=element_blank(),
        legend.title=element_blank(),
        legend.box="vertical")

ggsave("./fig_output_201215/eg_precid_cv_corrected_distribution.pdf", width = 18.5, height = 12, units = 'cm')


##### imputation 
complete_with_na <- function(data, sample, precursor){
  data %>%
    complete({{sample}}, {{precursor}})
}

impute_all_missing_log2 <- function(n_replicates, min, sd){
  set.seed(123)
  rnorm(n_replicates, mean = min - log2(3), sd = sd)
}

impute_missing_log2 <- function(n_replicates, mean, sd){
  set.seed(123)
  rnorm(n_replicates, mean = mean, sd = sd)
}

impute_DIA <- function(data, sample, precursor, intensity, condition){
  
  by <- enquo(sample)
  
  annotation <- data %>%
    distinct({{condition}}, {{sample}})
  
  replicates <- data %>%
    distinct({{condition}}, {{sample}})%>% 
    count({{condition}})%>%
    summarize(max(n))%>%
    as.integer()
  
  data%>%
    dplyr::select({{sample}}, {{precursor}}, {{intensity}})%>%
    complete_with_na({{sample}}, {{precursor}})%>%
    left_join(annotation, by = as_label(by))%>%
    group_by({{precursor}})%>%
    mutate(min = min({{intensity}}, na.rm = TRUE))%>%
    group_by({{precursor}}, {{condition}})%>%
    mutate(sd = sd({{intensity}}, na.rm = TRUE))%>%
    group_by({{precursor}})%>%
    mutate(sd = mean(sd, na.rm = TRUE))%>%
    filter(!is.nan(sd))%>%
    group_by({{precursor}}, {{condition}})%>%
    mutate(mean = mean({{intensity}}, na.rm = TRUE))%>%
    mutate(all_missing = if_else(is.nan(mean) == TRUE, TRUE, FALSE))%>%
    mutate(impute = ifelse(all_missing == TRUE, impute_all_missing_log2(replicates, min, sd), impute_missing_log2(replicates, mean, sd)))%>%
    mutate(normalised_intensity_imputed_log2 = ifelse(is.na({{intensity}}) == TRUE, impute, {{intensity}}))%>%
    mutate(imputed = is.na({{intensity}}))%>%
    dplyr::select(-impute, -mean, -sd, -min)%>%
    ungroup()
}

colnames(DIA_spectronaut_clean_na)

n_replicates <- DIA_spectronaut_clean_na %>% 
  distinct(r_condition, r_condrep) %>% 
  group_by(r_condition) %>% 
  count(r_condition) %>% 
  pull(n) %>% 
  unique()

conditions <- unique(as.character(DIA_spectronaut_clean_na$r_condition))


DIA_spectronaut_clean_na_imputed <-
  impute_DIA(
    DIA_spectronaut_clean_na,
    r_condrep,
    eg_precid,
    normalised_intensity_log2,
    r_condition
  ) %>%
  left_join(DIA_spectronaut_clean_na %>% distinct(eg_precid, r_condrep, normalised_intensity_log2, r_condition, pg_protein_groups), 
            by = c("eg_precid", "r_condrep", "normalised_intensity_log2", "r_condition"))


unique(DIA_spectronaut_clean_na_imputed[,c("r_condition", "r_condrep")])


colnames(DIA_spectronaut_clean_na_imputed)

unique(DIA_spectronaut_clean_na_imputed[is.na(DIA_spectronaut_clean_na_imputed[,"normalised_intensity_imputed_log2"]),"eg_precid"])

# checl log2 intensity distributions before/after imputationt
DIA_spectronaut_clean_na_imputed %>%
  dplyr::select(normalised_intensity_log2,normalised_intensity_imputed_log2) %>%
  pivot_longer(cols = everything(),
               names_to = "imputed",
               values_to = "intensity") %>%
  filter(!is.na(intensity)) %>%
  ggplot(aes(intensity, fill = imputed)) +
  labs(title = "Histogram of intensities before and after imputation (Log2)",
       x = "Log2 Intensity",
       y = "Frequency",
       fill = "Type") +
  geom_histogram(
    binwidth = 0.5,
    color = "black",
    position = "identity",
    alpha = 0.7
  ) +
  theme_bw()

ggsave("./fig_output_201215/histogram_intensities_corrected_AfterImputation.pdf", height=10, width=10, units='in', dpi=600)


# plot the protein profile plots after imputation
DIA_spectronaut_clean_na_imputed_top50_list <- DIA_spectronaut_clean_na_imputed %>%
  get_topN_peptides(., pg_protein_groups, eg_precid, normalised_intensity_imputed_log2, 50) %>% 
  split(.$pg_protein_groups)

plot_function2 <- function(x) {
  ggplot(x, aes(x=r_condrep, y=normalised_intensity_imputed_log2, group = eg_precid, shape = eg_precid, colour = eg_precid)) +
    ggtitle(unique(x$pg_protein_groups)) +
    geom_line(aes(colour = eg_precid)) +
    geom_point(aes(shape=eg_precid, size = imputed)) +
    scale_shape_manual(values = c(letters, LETTERS)) +
    theme_bw() + 
    theme(legend.position = "right", axis.text.x = element_text(angle = 90))+
    guides(shape = guide_legend(override.aes = list(size=3,linetype=0)))+
    theme(text = element_text(size=10),
          legend.key=element_blank(),
          legend.title=element_blank(),
          legend.box="vertical")}

prot_plots <- map(DIA_spectronaut_clean_na_imputed_top50_list, plot_function2)

ml <- marrangeGrob(prot_plots, nrow = 2, ncol = 1)
ggsave("./fig_output_201215/proteinplots_interesting_peptides_corrected_AfterImputation_filter.pdf", ml, height=20, width=20, units='in', dpi=600)




################## Statistical analysis  ###############
unique(DIA_spectronaut_clean_na_imputed$r_condition)
unique(DIA_spectronaut_clean_na_imputed$r_condrep)
DIA_spectronaut_clean_na_imputed_backup <- DIA_spectronaut_clean_na_imputed
# DIA_spectronaut_clean_na_imputed <- subset(DIA_spectronaut_clean_na_imputed, r_condrep %in% c("DMSO_1", "DMSO_2", "Chloroxine_1", "Chloroxine_2", "Tiabendozole_1", "Tiabendozole_2", "Suloctidil_1", "Suloctidil_2", "Carmofur_1", "Carmofur_2", "Vatalanib_1", "Vatalanib_2", "Paraquat_1", "Paraquat_2","Thiethylperazine_maleate_1", "Thiethylperazine_maleate_2", "Tegaserod_maleate_1", "Tegaserod_maleate_2", "Trimethoprim_1", "Trimethoprim_2"))

set1 <- c("Chloroxine10_Lip", "Mock_Lip", "Mock2_Lip")
set2 <- c("Chloroxine1_Lip", "Mock_Lip", "Mock2_Lip")
set3 <- c("Chloroxine0.1_Lip", "Mock_Lip", "Mock2_Lip")
set4 <- c("Chloroxine0.01_Lip", "Mock_Lip", "Mock2_Lip")
set5 <- c("Tiabendozole40_Lip", "Mock_Lip", "Mock2_Lip")
set6 <- c("Suloctidil20_Lip", "Mock_Lip", "Mock2_Lip")
set7 <- c("Thiethylperazine20_Lip", "Mock_Lip", "Mock2_Lip")
set8 <- c("Tegaserod20_Lip", "Mock_Lip", "Mock2_Lip")
colnames(DIA_spectronaut_clean)

set1b <- c("Chloroxine10_Lip_1", "Chloroxine10_Lip_2", "Chloroxine10_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set2b <- c("Chloroxine1_Lip_1", "Chloroxine1_Lip_2", "Chloroxine1_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set3b <- c("Chloroxine0.1_Lip_1", "Chloroxine0.1_Lip_2", "Chloroxine0.1_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set4b <- c("Chloroxine0.01_Lip_1", "Chloroxine0.01_Lip_2", "Chloroxine0.01_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set5b <- c("Tiabendozole40_Lip_1", "Tiabendozole40_Lip_2", "Tiabendozole40_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set6b <- c("Suloctidil20_Lip_1", "Suloctidil20_Lip_2", "Suloctidil20_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set7b <- c("Thiethylperazine20_Lip_1", "Thiethylperazine20_Lip_2", "Thiethylperazine20_Lip_3", "Mock_Lip_1", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")
set8b <- c("Tegaserod20_Lip_1", "Tegaserod20_Lip_2", "Tegaserod20_Lip_3", "Mock2_Lip_1", "Mock_Lip_2", "Mock2_Lip_2", "Mock_Lip_3", "Mock2_Lip_3")

# lump/sum the eg_precid to eg_modified_sequence
DIA_spectronaut_clean_na_imputed_pep <- DIA_spectronaut_clean_na_imputed %>%
  left_join(DIA_spectronaut_clean %>% distinct(eg_precid, eg_modified_sequence), by = "eg_precid") %>%
  group_by(eg_modified_sequence, r_condrep) %>%
  mutate(pep_imputed_log2 = sum(2^normalised_intensity_imputed_log2)) %>%
  distinct( pg_protein_groups, eg_modified_sequence, r_condrep, r_condition, pep_imputed_log2) %>%
  as_tibble()


write.table(DIA_spectronaut_clean_na_imputed_pep, "./data_output_201215/DIA_spectronaut_clean_na_imputed_pep.tsv", sep = "\t", quote = FALSE, row.names = FALSE)

DIA_spectronaut_clean_na_imputed_pep_matrix <- DIA_spectronaut_clean_na_imputed_pep %>% 
  pivot_wider(id_cols = c(pg_protein_groups, eg_modified_sequence), names_from = r_condrep, values_from = pep_imputed_log2)

write.table(DIA_spectronaut_clean_na_imputed_pep_matrix, "./data_output_201215/DIA_spectronaut_clean_na_imputed_pep_matrix.tsv", sep = "\t", quote = FALSE, row.names = FALSE)


DIA_pep_top50_list <- DIA_spectronaut_clean_na_imputed_pep %>%
  get_topN_peptides(., pg_protein_groups, eg_modified_sequence, pep_imputed_log2, 50) %>% 
  split(.$pg_protein_groups)

plot_function2 <- function(x) {
  ggplot(x, aes(x=r_condrep, y=pep_imputed_log2, group = eg_modified_sequence, shape = eg_modified_sequence, colour = eg_modified_sequence)) +
    ggtitle(unique(x$pg_protein_groups)) +
    geom_line(aes(colour = eg_modified_sequence)) +
    geom_point(aes(shape=eg_modified_sequence)) +
    scale_shape_manual(values = c(letters, LETTERS)) +
    theme_bw() + 
    theme(legend.position = "right", axis.text.x = element_text(angle = 90))+
    guides(shape = guide_legend(override.aes = list(size=3,linetype=0)))+
    theme(text = element_text(size=10),
          legend.key=element_blank(),
          legend.title=element_blank(),
          legend.box="vertical")}

prot_plots <- map(DIA_pep_top50_list, plot_function2)

ml <- marrangeGrob(prot_plots, nrow = 2, ncol = 1)
ggsave("./fig_output_201215/proteinplots_lumpedpeptides.pdf", ml, height=20, width=20, units='in', dpi=600)


